package com.devcamp.circle.restapi.model;

public class Circle {
    double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getArea(double radius) {
        return (Math.PI)* (radius)*radius;
    }

    public double getPerimeter() {
        return 2*(Math.PI)*radius;
    }

    public String toString() {
        return "Circle[radius = " + radius + "]";
    }
}
