package com.devcamp.circle.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circle.restapi.model.Circle;

@RestController
public class controller {
    @CrossOrigin
    @GetMapping("circle-area")
    public double circleArea() {
        double radius = 8;
        Circle circle1 = new Circle(radius);
        System.out.println(circle1.toString());
        return circle1.getArea(radius);
    }
}
